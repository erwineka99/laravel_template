@extends('adminlte.master')

@section('content')
    <div class="card">
        <a href="{{ route('pertanyaan.create')}}" class="btn btn-primary">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Judul</th>
                <th scope="col">Isi</th>
                <th scope="col">Tanggal dibuat</th>
                <th scope="col">Tanggal diperbaharui</th>
                <th scope="col">profil</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($pertanyaan as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->judul}}</td>
                        <td>{{$value->isi}}</td>
                        <td>{{$value->tanggal_dibuat}}</td>
                        <td>{{$value->tanggal_diperbaharui}}</td>
                        <td>{{$value->profil_id}}</td>
                        <td style="display:flex;">
                            <a href="{{route('pertanyaan.show',['pertanyaan'=>$value->id])}}" class="btn btn-info">Show</a>
                            <a href="{{route('pertanyaan.edit',['pertanyaan'=>$value->id])}}" class="btn btn-primary">Edit</a>
                            <form action="{{route('pertanyaan.destroy',['pertanyaan'=>$value->id])}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection
