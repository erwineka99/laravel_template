@extends('adminlte.master')

@section('content')
    <div class="card">
        <div class="card-header">
            <h3>Tambah Pertanyaan</h3>
        </div>
        <form action="{{route('pertanyaan.store')}}" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control" name="judul" id="judul" placeholder="Masukkan Judul">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">body</label>
                <input type="text" class="form-control" name="isi" id="isi" placeholder="Masukkan Isi">
                @error('isi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
    </div>
@endsection

@push('scripts')
<script>

</script>
@endpush
