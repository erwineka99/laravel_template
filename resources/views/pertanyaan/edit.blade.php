@extends('adminlte.master')

@section('content')
    <div class="card">
        <div class="card-header">
            <h3>Edit Pertanyaan</h3>
        </div>
        <form action="{{route('pertanyaan.update',['pertanyaan'=>$pertanyaan->id])}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control" name="judul" value="{{$pertanyaan->judul}}" id="isi" placeholder="Masukkan Title">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">body</label>
                <input type="text" class="form-control" name="isi"  value="{{$pertanyaan->isi}}"  id="isi" placeholder="Masukkan Body">
                @error('body')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
@endsection

@push('scripts')
<script>

</script>
@endpush
