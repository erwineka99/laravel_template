<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pertanyaan extends Model
{
    protected $table = "pertanyaan";

    protected $fillable = ['judul','isi','tanggal_dibuat','tanggal_diperbaharui','profil_id'];
    public $timestamps = false;

}
