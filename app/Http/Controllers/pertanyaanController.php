<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\pertanyaan;

class pertanyaanController extends Controller
{
    function index(){
        //$pertanyaan = DB::table('pertanyaan')->get();

        //eloquent
        $pertanyaan=pertanyaan::all();
        return view('pertanyaan.index', compact('pertanyaan'));
    }

    function create(){

        return view('pertanyaan.create');
    }

    function store(Request $request){
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required',
        ]);

        // $pertanyaan=new pertanyaan();
        // $pertanyaan->timestamps = false;
        // $pertanyaan->judul=$request["judul"];
        // $pertanyaan->isi=$request["isi"];
        // $pertanyaan->tanggal_dibuat=date("Y-m-d");
        // $pertanyaan->tanggal_diperbaharui=date("Y-m-d");
        // $pertanyaan->profil_id=1;
        // $pertanyaan->save();

        // $query = DB::table('pertanyaan')->insert([
        //     "judul" => $request["judul"],
        //     "isi" => $request["isi"],
        //     "tanggal_dibuat" => date("Y-m-d"),
        //     "tanggal_diperbaharui" => date("Y-m-d"),
        //     "profil_id" => 1
        // ]);

        // insert data mass assignment

        $pertanyaan=pertanyaan::create(['judul' => $request["judul"] ,'isi'=>$request["isi"],'tanggal_dibuat'=>date("Y-m-d"),'tanggal_diperbaharui'=>date("Y-m-d"),"profil_id"=>1]);

        return redirect('/pertanyaan');
    }

    function show($pertanyaan_id){
        //$pertanyaan = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        $pertanyaan = pertanyaan::find($pertanyaan_id);

        return view('pertanyaan.show', compact('pertanyaan'));
    }

    function edit($pertanyaan_id){
        $pertanyaan = pertanyaan::find($pertanyaan_id);
        return view('pertanyaan.edit', compact('pertanyaan'));
    }

    function update($pertanyaan_id, Request $request){
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
        ]);
        //mass update
        pertanyaan::where('id', $pertanyaan_id)
          ->update(['judul' =>  $request["judul"],
                    'isi' =>  $request["isi"],
                    'tanggal_diperbaharui' =>  date("Y-m-d"),
                    ]);


        // $query = DB::table('pertanyaan')
        //     ->where('id',$pertanyaan_id)
        //     ->update([
        //     "judul" => $request["judul"],
        //     "isi" => $request["isi"],
        //     "tanggal_diperbaharui" => date("Y-m-d")
        // ]);
        return redirect('/pertanyaan');
    }

    function destroy($pertanyaan_id){
        //$query = DB::table('pertanyaan')->where('id', $pertanyaan_id)->delete();
        //deleteing an existing model on key

        pertanyaan::destroy($pertanyaan_id);

        return redirect('/pertanyaan');
    }


}
